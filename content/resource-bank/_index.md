---
title: Resource Bank
draft: false
menu: main
weight: 80
summary: This page provides a way to quickly search for resources using related keywords.
bannerHeading: Resource Bank
bannerText: This page provides a way to quickly search for resources using related keywords.
---

{{< resource_bank_search >}}
{{< hyperlink_title name="Introduction" >}}
<div class="grid-row">
  <div class="grid-col">
    {{< hyperlink url="https://www.fmcsa.dot.gov/mission/policy/map-21-moving-ahead-progress-21st-century-act" name="Moving Ahead for Progress in the 21st Century (MAP-21)" tags="map21, map-21, moving ahead for progress in the 21st century, bill, law, transportation bill">}}
    {{< hyperlink url="https://www.fhwa.dot.gov/fastact/" name="Fixing America's Surface Transportation Act (FAST Act)" tags="fast act, fixing america's surface transportation act, fast, bill, law, transportation bill, fhwa">}}
    {{< hyperlink url="https://www.cms.gov/Medicare-Medicaid-Coordination/Fraud-Prevention/Medicaid-Integrity-Program/Education/Non-Emergency-Medical-Transport" name="Non-Emergency Medical Transportation (NEMT)" tags="nemt, non-emergency medical transportation, medical">}}
    {{< hyperlink url="https://www.transit.dot.gov/coordinating-council-access-and-mobility" name="Coordinating Council on Access and Mobility (CCAM)" tags="ccam, coordinating council on access and mobility, access, mobility, fta">}}
    {{< hyperlink url="https://www.transit.dot.gov/ccam/about/transportation-coordination" name="Transportation Coordination (FTA)" tags="coordination, transportation coordination, fta">}}
    {{< hyperlink url="https://www.transit.dot.gov/regulations-and-guidance/ccam/about/ccam-program-inventory" name="Coordinating Council on Access and Mobility (CCAM) - Programs" tags="ccam, coordinating council on access and mobility, access, mobility, fta, coordinating programs, programs">}}
    {{< hyperlink url="https://www.transit.dot.gov/ntd" name="National Transit Database (NTD)" tags="ntd, national transit database, fta">}}
    {{< hyperlink url="https://hfs.illinois.gov/" name="Illinois Department of Healthcare and Family Services (IDHFS)" tags="idhfs, hfs, illinois department of healthcare and family services, illinois">}}
    {{< hyperlink url="https://www.congress.gov/bill/117th-congress/house-bill/3684/text" name="Infrastructure Investment and Jobs Act (IIJA)" tags="iija, infrastructure investmant and jobs act, bill, law, transportation bill">}}
    {{< hyperlink url="https://www.transit.dot.gov/funding/grants/enhanced-mobility-seniors-individuals-disabilities-section-5310" name="Enhanced Mobility for Seniors and Individuals with Disabilities" tags="5310, section 5310, enhanced mobility for seniors and individuals with disabilities, bill, law, funding, transportation bill, fta">}}
    {{< hyperlink url="https://www.healthcareconsumers.org/" name="Champaign County Healthcare Consumers (CCHCC)" tags="healthcare, health care, health, champaign county healthcare consumers, cchcc">}}
    {{< hyperlink url="https://www.eciaaa.org/" name="East Central Illinois Area Agency on Aging (ECIAAA)" tags="eciaaa, aging, illinois, east central illinois area agency on aging">}}
    {{< hyperlink url="https://www.eciaaa.org/index.php/consumer-resources/all-services" name="East Central Illinois Area Agency on Aging (ECIAAA) - Services" tags="eciaaa, aging, illinois, east central illinois area agency on aging, services">}}
    {{< hyperlink url="https://dres.illinois.edu/" name="Division of Disability Resources and Educational Services (DRES)" tags="dres, disability, division of disability resources and educational services, uiuc">}}
    {{< hyperlink url="https://www.dsc-illinois.org/" name="Developmental Services Center (DSC)" tags="dsc, disability, illinois, developmental services center">}}
  </div>
</div>

{{< hyperlink_title name="Mobility Today" >}}
<div class="grid-row">
  <div class="grid-col">
    {{< hyperlink url="https://mtd.org/" name="Champaign-Urbana Mass Transit District homepage" tags="champaign-urbana, transit, bus, transportation, local">}}
    {{< hyperlink url="https://mtd.org/riding/mtd-services/can-you-tell-me-about-your-ada-services/" name="MTD ADA Paratransit FAQ" tags="disability, transit, paratransit, ada, mtd, bus">}}
    {{< hyperlink url="https://mtd.org/riding/accessibility/how-do-i-apply-for-ada-paratransit/" name="MTD ADA Paratransit Application" tags="disability, transit, paratransit, ada, mtd, bus, application">}}
    {{< hyperlink url="https://c-carts.com/" name="C-CARTS homepage" tags="transit, rural transit, bus, c-carts, champaign county">}}
    {{< hyperlink url="https://ccrpc.org/programs/hstp/human-service-transportation-plan-region-8-directory/" name="HSTP Transportation Directory" tags="transit, transportation, hspt, directory, service, services, bus">}}
  </div>
</div>

{{< hyperlink_title name="Mobility Tomorrow" >}}
<div class="grid-row">
  <div class="grid-col">
    {{< hyperlink url="https://journals.sagepub.com/doi/10.1177/03611981221150917" name="“GIS-Based Approach to Dynamic Accessibility: Incorporating a User Perspective to Recognize Social Equity Implications,” 2022" tags="research, equity, transit research, transportation, late-shift">}}
    {{< hyperlink url="https://www.osfhealthcare.org/heart-of-mary/amenities/social-services/" name="Faith in Action" tags="volunteer, faith in action, osf">}}
    {{< hyperlink url="https://mtd.org/riding/accessibility/how-do-i-apply-for-ada-paratransit/" name="MTD ADA Paratransit Application" tags="disability, transit, paratransit, ada, mtd, bus, application">}}
  </div>
</div>

{{< hyperlink_title name="Needs, Gaps, Barriers, Duplications" >}}
<div class="grid-row">
  <div class="grid-col">
    {{< hyperlink url="https://adata.org/factsheet/ADA-accessible-transportation" name="“The ADA and Accessible Ground Transportation,” ADA National Network" tags="research, ada, americans with disabilities act, disability, accessibility">}}
    {{< hyperlink url="https://www.kidney.org/sites/default/files/v40a_a4.pdf" name="“Need a Ride?,” National Kidney Foundation Journal of Hematology Social Work" tags="research,">}}
    {{< hyperlink url="https://www.aarp.org/livable-communities/network-age-friendly-communities/info-2023/senior-friendly-transportation.html" name="“5 A’s of Senior-Friendly Transit,” AARP" tags="aarp, senior friedly, older adult, age">}}
    {{< hyperlink url="https://www.apta.com/wp-content/uploads/Further-Investment-In-Public-Transit-Can-Meet-Growing-Needs-Of-Late-Shift-Workers-press-release.pdf" name="“Further Investment in Public Transit Can Meet Growing Needs of Late-Shift Workers,” American Public Transportation Association" tags="research, apta">}}
    {{< hyperlink url="https://nationalcenterformobilitymanagement.org/oc-oc-trip-booking/" name="“Trip Booking,” National Center for Mobility Management" tags="ncmm, research">}}
    {{< hyperlink url="https://mtd.org/media/3770/ada-application.pdf" name="MTD ADA Paratransit eligibility application" tags="mtd, ada, paratransit, application, disability">}}
  </div>
</div>