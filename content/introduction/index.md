---
title: "Introduction"
draft: false
menu: main
summary: "The Human Service Transportation Planning (HSTP) program is an initiative to evaluate existing transportation services in the Champaign-Urbana urbanized area, identify the transportation needs of individuals with disabilities and older adults, and establish goals and strategies for meeting these needs within our community."
weight: 10
tags: 

bannerHeading: Introduction
bannerText: This page introduces our community's Human Service Transportation Plan program.
bannerUrl: banner.jpg

---
[The Human Service Transportation Planning (HSTP) program](https://ccrpc.org/divisions/planning_and_development/region_8_human_service_transportation_plan_committee/index.php) is an initiative to evaluate existing transportation services in the Champaign-Urbana urbanized area, identify the transportation needs of individuals with disabilities and older adults, and establish goals and strategies for meeting these needs within our community. The overarching goal of the HSTP program is to meet coordination requirements set forth in federal and state policies, which together guarantee funding for the continuation of safe, affordable and reliable human service transportation options in our community.

#### Resources mentioned in this section:
- [Champaign County Regional Planning Commission's Human Service Transportation Planning (HSTP) program](https://ccrpc.org/programs/hstp/)
- [East Central Illinois Area Agency on Aging (ECIAAA) homepage](https://www.eciaaa.org/)
- [ECIAAA list of aging services](https://www.eciaaa.org/index.php/consumer-resources/all-services)
- [Illinois Department of Healthcare and Family Services (IDHFS) homepage](https://hfs.illinois.gov/)
- [Champaign County Healthcare Consumers homepage](https://www.healthcareconsumers.org/)
- [Information on the Non-Emergency Transportation Services Prior Authorization Program (NETSPAP)](https://www.findhelp.org/illinois-department-of-healthcare-and-family-services-%2528hfs%2529--springfield-il-non-emergency-transportation-services-prior-authorization-program-%2528netspap%2529/6183691299848192?postal=60074&ref=ab_redirect) via [FindHelp.org](findhelp.org)
- [University of Illinois Division of Disability Resources and Educational Service (DRES) homepage](https://dres.illinois.edu/)
- [Developmental Services Center (DSC) homepage](https://www.dsc-illinois.org/)

<p style="text-align:center">
  <img src="hstp-region-map.jpg" width="45%" />
</p>

## What is human service transportation?
Human service transportation refers to transportation services made specifically for people with physical disabilities, people with cognitive disabilities, older adults, people with serious health conditions, and those with lower incomes. Human services transportation is different from emergency transportation, such as ambulance services. Human service transportation is also different from public transit for the general public, even though some forms of human service transportation may be taxpayer-funded and open to use by any member of the public.

## Why is human service transportation its own category?
As a requirement of SAFETEA-LU, grantees under Section 5310: Enhanced Mobility of Seniors and Individuals with Disabilities, Section 5316: Job Access and Reverse Commute (JARC), and Section 5317: New Freedom grant programs were required to be part of a “locally developed coordinated public transit human service transportation plan” to gain eligibility for transportation funding for federal fiscal year 2007 and beyond. Over the years, the HSTP program has been extended by each federal transportation bill as a means of enhancing access and quality through coordination.

In 2007, the Champaign-Urbana Urbanized Area HSTP group brought together transportation providers, human service agencies, riders, and members of the community to improve efficiency and equity of transportation services within the urbanized area, as well as address existing service barriers. The Champaign-Urbana Urbanized Area Transportation Study (CUUATS) Policy and Technical Committees adopted the first Champaign-Urbana Urbanized Area HSTP on January 23rd, 2008. The first update of this plan was adopted by the CUUATS Policy and Technical Committees on December 12th, 2012. Between 2012 and now, the plan was updated once in 2018.

Each state receives Federal Transit Administration (FTA) funds for the programs subject to the HSTP coordination process, and each state is required to determine how best to meet the mandate for coordination. The Illinois Department of Transportation – Office of Intermodal Project Implementation (IDOT-OIPI), formerly the Department of Public and Intermodal Transportation (DPIT), established eleven HSTP regions and a coordinator for each. The Champaign County Regional Planning Commission (CCRPC) is the Metropolitan Planning Organization (MPO) for the Champaign-Urbana Urbanized Area. CCRPC staff are required to develop the Champaign-Urbana Urbanized Area HSTP, and they facilitate the same process for [HSTP Region 8](https://ccrpc.org/programs/hstp/) (rural transit).

## Major plan components
The Champaign-Urbana Urbanized Area HSTP consists of six major components:
- An overview of current transportation legislation and funding;
- Input from public participation and community engagement;
- A report on existing conditions and demographics of the urbanized area;
- A view of mobility today, analyzing major trip generators, top employers, and existing transportation services within the urbanized area;
- A detailed summary of coordination, gaps, unmet needs, and duplication of services; and
- A vision for mobility tomorrow, including goals, objectives and specific targets for the urbanized area.

## Transportation legislation and funding

### MAP-21
On July 6th, 2012, SAFETEA-LU was replaced with the authorization of a two-year federal transportation bill named [Moving Ahead for Progress in the 21st Century](https://www.fmcsa.dot.gov/mission/policy/map-21-moving-ahead-progress-21st-century-act) (MAP-21), covering federal fiscal years 2013 and 2014. MAP-21 reaffirmed the statute mandating local coordination of transportation services and consolidated some of the funding programs affected by these requirements. Job Access and Reverse Commute (JARC), formerly Section 5316, no longer existed as a separate program, but funding for these activities was available under both Urbanized Area Formula Grants (Section 5307) and Formula Grants for Rural Areas (Section 5311); and New Freedom, formerly Section 5317, was absorbed by Section 5310. As a result, Section 5307 and Section 5311 providers were required to participate in the HSTP process. MAP-21 was originally set to expire September 30th, 2014; however, five extensions allowed the bill to remain in effect until December 4th, 2015.

### FAST Act
The five-year [Fixing America’s Surface Transportation Act](https://www.fhwa.dot.gov/fastact/) (FAST Act) Bill for federal fiscal years 2016 through 2020 was authorized by President Obama on December 4th, 2015. The FAST Act includes roughly one billion dollars per year in increases across all transit funding streams. The FAST Act also reintroduced a Discretionary Bus and Bus Facilities Program (Section 5339), available to 5307 and 5311 recipients.

### IIJA: Current funding
The [Infrastructure Investment and Jobs Act](https://www.congress.gov/bill/117th-congress/house-bill/3684/text) (IIJA) is a major bipartisan infrastructure bill that was signed into law by President Joe Biden on November 15th, 2021. The legislation includes $1.2 trillion in funding for a wide range of infrastructure projects, including transportation, broadband, and water systems. In particular, the IIJA allocates $110 billion to address the country’s aging transportation infrastructure, including $39 billion for public transit.

The IIJA’s impact on public transit funding is significant, as it represents an increase in federal investment in public transportation. The funding is expected to be used to improve existing transit systems, expand service to underserved communities, and modernize transit infrastructure. This includes upgrading buses and rail cars, investing in electric and low-emission buses, and improving accessibility for people with disabilities. The IIJA’s commitment to public transit funding reflects a growing recognition of the importance of sustainable, efficient transportation systems in promoting economic growth and reducing greenhouse gas emissions.

### Section 5310 changes and continued funding status
[Enhanced Mobility for Seniors and Individuals with Disabilities](https://www.transit.dot.gov/funding/grants/enhanced-mobility-seniors-individuals-disabilities-section-5310) (Section 5310) provides funding for programs beyond traditional public transportation and ADA paratransit service to meet the specific needs of seniors and persons with disabilities. Section 5310 remains largely unaltered by IIJA. A minimum of 55% of funds must be allocated for capital projects such as the procurement of ADA accessible buses and vans, vehicle maintenance, purchase of service, computer hardware and software, etc. The other 45% of program funds may be used for other projects, such as those originally targeted by the New Freedom program: travel trainings, sidewalks, improved signage, way-finding technology, etc. The goals of Section 5310 are to maintain a safe fleet of vehicles to service transportation needs of the indicated target populations, to support the continuation and growth of existing services, and to foster the growth of new services.

Funding is allocated to state Departments of Transportation (DOTs) for rural and small urban (population under 200,000) based on each state’s population of the two target groups for this program. For large, urbanized areas (population over 200,000), the Governor selects a designated direct recipient. In Illinois, Section 5310 funding is primarily used to finance the Consolidated Vehicle Procurement (CVP) program, which provides vehicles at no cost to the grantee and is funded by 80% federal funds and a 20% state match. Subrecipients of these funds within the Champaign-Urbana urbanized area are part of the IDOT Region 8 HSTP program and they include local government authorities that operate public transit, rural mass transit districts, and private non-profit organizations.

Although Section 5310 has been largely unaltered by ILJA legislation, one notable provision (Section 3006) created a discretionary “pilot program for innovative coordinated access and mobility,” which provides funding for innovations in coordination of transportation for disadvantaged populations, [Non-Emergency Medical Transportation](https://www.cms.gov/Medicare-Medicaid-Coordination/Fraud-Prevention/Medicaid-Integrity-Program/Education/Non-Emergency-Medical-Transport) (NEMT) services, and coordination technology such as one-call or one-click centers. This provision also calls on the federal interagency [Coordinating Council on Access and Mobility](https://www.transit.dot.gov/coordinating-council-access-and-mobility) (CCAM) to create and update a strategic plan on transportation coordination between federal agencies, including proposed changes to federal laws and regulations that currently hinder transportation coordination at the local level.

In March 2022, FTA posted guidance clarifying coordination on human service transportation on a new [transportation coordination](https://www.transit.dot.gov/ccam/about/transportation-coordination) webpage. Coordinated transportation involves multiple entities working together to deliver one or more components of a transportation service to increase capacity. The transportation coordination guidance aims to reduce overlap between the [130 CCAM programs](https://www.transit.dot.gov/regulations-and-guidance/ccam/about/ccam-program-inventory) across nine agencies that may fund human service transportation and incentivize collaboration by clarifying eligible reporting into the [National Transit Database](https://www.transit.dot.gov/ntd) (NTD). This new guidance addresses the following topics as they relate to NTD reporting: definition of public transportation; paratransit; charter service; incidental use of transit assets; and trip brokering.

### Section 5307 changes and continued funding status
Section 5307 (also called Urbanized Area Formula Grants) serves the same purpose as Section 5311, but for areas with 50,000 or more residents and designated as “urbanized areas” by the United States Census Bureau. Champaign-Urbana is considered a small urbanized area because its population is under 200,000. In this case, FTA funds are distributed to the Governor, and apportioned to the subrecipient public transportation provider: Champaign-Urbana Mass Transit District (MTD).

### Urbanized vs. rural funding eligibility
MTD submits requisitions and coordinate vehicle purchases directly with the FTA; however, all reporting must be submitted to IDOT, and MTD is subject to compliance reviews conducted or contracted by IDOT. 

### State funding and local match
The State of Illinois provides funding for all public transportation providers, regardless of population, through the Downstate Operating Assistance Program (DOAP). DOAP helps recipients with operations costs and improvements to public transportation services in the urban and rural areas of downstate Illinois. For most operators, DOAP is the primary source of reimbursement for operating and administrative expenses. 

## Other transportation funding 
In addition to funding mechanisms dedicated specifically to transit, numerous funding sources exist at various federal, state, and local levels through several programs and initiatives that may be applied to transportation services.

### Coronavirus Aid, Relief, and Economic Security (CARES) Act
The Coronavirus Aid, Relief, and Economic Security (CARES) Act was passed in 2020 and is the newest source of non-transportation-specific funding that has been utilized by human service agencies locally. CARES provided significant funding for human services across the US in the wake of the Covid-19 pandemic. The funding helped human service organizations and local governments maintain essential services and provide support for vulnerable populations, including people with disabilities, people experiencing cognitive decline, older adults, and people with significant health issues. It also enabled the purchase of personal protective equipment (PPE) and other supplies necessary to ensure the safety of drivers and passengers. Additionally, in the public transit space, CARES funding supported the implementation of innovative solutions to address the challenges posed by the pandemic, such as the adoption of contactless fare payment systems and the provision of on-demand transportation services. Overall, the CARES Act played a critical role in mitigating the impact of the pandemic on human service organizations, helping to ensure that care and support remained available to those who needed it most. Locally, CARES funding has been used to purchase security equipment for C-CARTS buses, meeting a critical need expressed in C-CARTS user survey responses.

### Social Security Act Title XIX- Medicaid transportation funding
The [Illinois Department of Healthcare and Family Services](https://hfs.illinois.gov/) (IDHFS) contracts with First Transit, Inc. to provide the Non-Emergency Transportation Services Prior Authorization Program (NETSPAP) and brokerage for Medicaid funded transportation. IDHFS maintains the requirements and regulations for transportation providers to become Medicaid certified, and First Transit is the call center that approves all transportation funded by Medicaid. This funding becomes particularly vital in rural transit due to long-distance trips for specialized medical services, as these span multiple service areas in many cases.

For more information on Medicaid and transportation reimbursement, visit [Champaign County Healthcare Consumers](https://www.healthcareconsumers.org/) homepage (also available in Español) or reach them by phone at (217) 352-6533.

### Older Americans Act Title IIIB/Area Agency on Aging transportation funding
The federal Older Americans Act of 1965 (OAA) provides funding for a variety of in-home and community-based services to enhance quality of life, maintain independence, and assist with aging in place. A large contributor to the success of these goals is transportation. Title I and Title II of the OAA created the Administration on Aging (AOA) and declared its objectives, and Title III established ‘Grants for State and Community Programs on Aging’, which includes transportation. Funding for Title III programs is distributed based on each state’s population of individuals over the age of 60. It is then up to each state to apportion its funding to area agencies on aging. President Obama signed the Older Americans Act Reauthorization into law April 19th, 2016, guaranteeing funding through fiscal year 2019. In 2020, President Biden signed the Supporting Older Americans Act of 2020 which reauthorized funding through fiscal year 2024.

The [East Central Illinois Area Agency on Aging](https://www.eciaaa.org/) (ECIAAA) provides in-home and community-based services in Champaign County, including adult day services, adult protection services, comprehensive care coordination, congregate meals, and respite services. Transportation falls under ECIAAA’s Access Services, a “network of 12 Coordinated Points of Entry to provide information and assistance; and coordination with 6 Care Coordination Units and public and private transportation providers.” For a full listing of ECIAAA’s services, visit their [listing of services](https://www.eciaaa.org/index.php/consumer-resources/all-services).

### Service contracts and associated Human Service Transportation Program funding
Human service agencies serve vital direct-service roles for their consumers but can also contract with other local organizations to help meet those organizations’ clients’ transportation needs. This type of coordination is known as a service contract. The primary focus of the HSTP is to increase coordination between public transportation providers and human service agencies so consumers have affordable and reliable access to the services they need. In addition to the benefits for consumers, service contracts are fiscally advantageous to service providers on both sides. While the human service agency benefits in cost-savings and/or convenience, the public transit operator also benefits from being able to show increased ridership, Additionally, the transit operator can use the contract revenue as local match to access more Downstate Operating Assistance Program (DOAP) funding from the state. Additional benefits include:

- Pooling resources for a reduction in underutilization;
- Utilization of economies of scale for increased efficiency;
- Elimination of unnecessary competition for scarce resources;
- Better use of deadhead time;
- Attainment of skills or services without long-term commitment;
- Solution to agency limitations;
- Overall reduction in transit system cost per trip; and
- Ability of human service agencies to spend more time on core services.

In addition to community outing trips, these agencies provide transportation to get consumers to and from services. Ideally, human service agencies would spend their time solely providing direct-service to their clients and transportation would be provided by agencies whose specialty is transportation (e.g., a public transit agency). Later sections of this plan will discuss the steps that need to be taken to achieve this service model in our community.

Urbanized Area human service agencies, such as the University of Illinois [Division of Disability Resources and Educational Services](https://dres.illinois.edu/) (DRES) and the [Developmental Services Center](https://www.dsc-illinois.org/) (DSC), utilize service contracts by providing contracted transportation for MTD. DRES provides door-to-door transportation services to University of Illinois Urbana-Champaign students with disabilities. DSC provides transportation to people with disabilities throughout the Champaign-Urbana area.
