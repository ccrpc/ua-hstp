---
title: Home
draft: false
weight: 30
bannerHeading: Human Services Transportation Planning
bannerText: Human Service Transportation Planning refers to specialized transportation services for people with disabilities, older adults, and people with low incomes in the Champaign-Urbana Urban Area.
bannerUrl: banner.jpg
---

The Human Services Transportation Planning (HSTP) program in the Champaign-Urbana Urban Area aims to assess current transportation services, identify the needs of individuals with disabilities, older adults, and people with disabilities, and establish goals and strategies to meet these needs. The program is driven by the overarching goal of ensuring coordination compliance with federal and state policies to secure funding for safe, affordable, and reliable human services transportation options in the community. Mandated by SAFETEA-LU, grantees under specific federal grant programs had to participate in a locally developed coordinated public transit human services transportation plan to be eligible for transportation funding from 2007 onward.

Initiated in 2007, the Champaign-Urbana Urban Area HSTP brought together various stakeholders, including transportation providers, human services agencies, riders, and community members, to enhance the efficiency and equity of transportation services and address existing barriers. The plan undergoes periodic updates, with the first adopted in 2008, followed by updates in 2012 and 2018. The Illinois Department of Transportation, through its Office of Intermodal Project Implementation, divides the state into eleven HSTP regions, assigning a coordinator to each. The Champaign County Regional Planning Commission, as the Metropolitan Planning Organization for the area, is responsible for developing the Champaign-Urbana Urban Area HSTP, covering components such as legislative and funding overview, public input, existing conditions, current mobility analysis, coordination assessment, and a vision for future mobility.

On October 26th, 2023, the [Age-Friendly Champaign-Urbana Steering Committee](https://ahs.illinois.edu/chad-outreach), which oversaw this plan's creation, approved this web plan. See the letter below for details.

# Age-Friendly Champaign-Urbana Steering Committee Approval Letter

<iframe src="Age-Friendly-HSTP Approval Letter.pdf" width="100%" height="600px"></iframe>