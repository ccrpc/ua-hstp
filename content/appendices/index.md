---
title: Appendices
draft: false
menu: main
weight: 70
summary: View a list of appendices to this plan.
bannerHeading: Appendices
bannerText: This page contains a list of appendices to this plan.
bannerUrl: banner.jpg
---

## Appendix A: Transportation consumer survey results
Between May and August of 2023, surveys were distributed to riders of specialized transportation services in the Champaign-Urbana Urban Area, including MTD's ADA Paratransit service and UIUC's Disability Education Resources and Educational Services (DRES) transportation service. This section will summarize the results of the surveys that were collected.

### Survey results
The survey initiative conducted in the Champaign-Urbana area successfully gathered comprehensive data from both specialized transportation users and providers, shedding light on various aspects of the transportation system. From the consumer survey, valuable insights emerged, indicating overall satisfaction among riders, particularly those with disabilities, while also highlighting the need for improved communication and affordability measures. On the other hand, the survey of transportation providers offered essential information about ridership patterns, financial sustainability, and specific service details. These combined survey results offer a holistic understanding of the specialized transportation landscape, empowering stakeholders to identify opportunities for service enhancement, cost optimization, and increased accessibility for people with disabilities and those with low incomes. The synthesis of these survey findings lays the groundwork for informed decision-making, fostering collaborations, and driving positive changes to create a more inclusive and responsive specialized transportation network for the community.

#### Transportation consumer survey
After  collecting and analyzing the survey responses from specialized transportation providers in the Champaign-Urbana area, the results offer a wealth of valuable insights into specialized transportation users' perceptions of the transportation options that are available. The survey garnered responses from a diverse range of riders, but the primary respondents were people with disabilities and older adults. Since these are two of the target poopulations of human service transportation, it was important to ensure that their voices were at the forefront of the findings. The comprehensive data obtained through the paper and digital surveys, as well as the strategic advertising throughout specialized transportation facilities in Champaign-Urbana, has provided a comprehensive overview of the strengths, challenges, and opportunities within the current specialized transportation system.

The survey results revealed that a significant proportion of respondents expressed overall satisfaction with the existing specialized transportation services. They appreciated the convenience, reliability, and accessibility that exists, particularly in catering to the unique needs of people with disabilities. However, the survey also shed light on some areas that require improvement. Several respondents highlighted the need for enhanced communication channels with the transportation providers, suggesting that clearer information about schedules, routes, and service updates would benefit riders.

Additionally, the survey results underscored the importance of affordability in specialized transportation services. While many respondents acknowledged the efforts made to accommodate people with low income , there were calls for further financial support and reduced fares to increase accessibility for those facing economic challenges. These insights will be instrumental in guiding policymakers and transportation providers toward implementing more equitable fare structures and subsidy programs, ensuring that specialized transportation remains affordable for all who rely on it. Overall, the survey results present an invaluable roadmap for refining and expanding specialized transportation services in the Champaign-Urbana area to better meet the diverse needs of the community and enhance the overall quality of life for its residents.

#### Transportation provider survey
In parallel with the survey efforts targeting specialized transportation users, a separate survey was conducted to gather crucial information from the transportation providers themselves. This additional survey aimed to gain insights into the operational aspects of specialized transportation services in the Champaign-Urbana area. Transportation providers were asked to provide data related to ridership, financial information, and specific details about the types of services they offer.

By collecting data on ridership, the survey sought to understand the demand for specialized transportation services and identify any patterns or trends in usage. This information would be vital in gauging the effectiveness of existing services and anticipating future needs. Analyzing ridership data could also help identify peak usage periods, optimize route planning, and ensure that services are tailored to meet the demand effectively.

The survey also delved into the financial aspects of specialized transportation provision. By gathering financial information from transportation providers, such as operating costs and funding sources, a comprehensive overview of the financial landscape could be established. This insight could enable stakeholders to evaluate the sustainability of the existing services and explore opportunities for cost optimization and potential fare adjustments. Understanding the financial challenges faced by transportation providers would facilitate informed decision-making and pave the way for more efficient allocation of resources.

Furthermore, the survey sought to capture specific details about the kind of service each transportation provider offers. This included aspects like service coverage area, operating hours, vehicle types, and accommodation for people with disabilities. This granular data would provide a comprehensive understanding of the different service models in place and their respective strengths and limitations. Analyzing these details would aid in identifying best practices, potential areas for service improvement, and opportunities for collaboration and standardization across transportation providers to enhance the overall quality and accessibility of specialized transportation services in the Champaign-Urbana area.

Only two Champaign-Urbana Urban Area specialized transportation providers returned surveys, so transportation provider survey insights are limited. Click [here](https://docs.google.com/spreadsheets/d/1P-YxEG5xuU-oGzCZtQF4yi1-ntmWiioD3uz7FQMIS0Q/edit?usp=sharing) to view the responses in a Google Sheet.

### Survey results analysis

#### Transportation consumer survey
Surveying transit riders on their most frequently used transportation services is of utmost importance as it provides critical insights into the preferences and needs of the community. Understanding which transportation options are utilized most often enables transportation authorities and policymakers to prioritize and allocate resources effectively. Moreover, gathering this information facilitates a comprehensive understanding of the commuting patterns and travel behavior of the community, which is essential for devising strategies to improve the overall efficiency, accessibility, and sustainability of the transportation network.

<rpc-chart url="transportation-service-used-most.csv"
    chart-title="What transportation service do you use most often in the community?"
    type="pie"
    stacked="true"
    aspect-ratio="4"></rpc-chart>

<rpc-chart url="other-transportation-services.csv"
    chart-title="Do you use any other transportation services in Champaign-Urbana, in addition to the one you use most often?"
    type="pie"
    stacked="true"
    aspect-ratio="4"></rpc-chart>

By understanding how often transit riders utilize transportation services, authorities can gauge the demand and popularity of specific modes of transit. This information is essential for optimizing service schedules and ensuring that transportation resources are allocated efficiently during peak periods. Additionally, assessing the frequency of service usage helps identify potential gaps in coverage or areas where service improvements may be warranted. It allows transportation providers to assess the overall effectiveness of their service and make data-driven decisions to enhance riders' experiences. Furthermore, tracking the frequency of transportation service use over time enables the evaluation of the impact of any implemented changes or improvements, aiding in continuous refinement and adaptation to evolving community needs.

In the 2018 iteration of the Champaign-Urbana Urban Area HSTP Plan, 29 percent of survey respondents said they did use more than one transportation service, and 71 percent of survey respondents said they only use one service. In both the 2018 and 2023 iterations of this plan, specialized transportation users are largely reliant on solely one source of specialized transit.

<rpc-chart url="use-frequency.csv"
    chart-title="How often do you use transportation services?"
    type="pie"
    stacked="true"
    aspect-ratio="4"></rpc-chart>

Understanding the areas where riders feel limited in their mobility provides valuable feedback for transportation planners to identify gaps in service coverage and address accessibility issues. This data can guide the expansion of routes and services to reach underserved areas, ensuring that all members of the community have equal access to essential destinations like schools, healthcare facilities, workplaces, and recreational spots. By actively seeking feedback on travel limitations, transportation authorities can work towards creating a more comprehensive and equitable transit network, ultimately enhancing the quality of life for residents and fostering a vibrant and connected community.

In 2018, 28 percent of respondents said they use their agency's transportation services less than once a month, 24 percent used it 4-5 days a week, 21 percent used it about once a month, 16 percent used it about once per week, and 11 percent used it 2-3 days per week. Overall, the 2023 iteration survey respondents use transit more often than the 2018 survey respondents. This could be partly due to the fact that in 2023, most surveys were completed on board a transit vehicle and survey distributers focused most on on-board survey distribution. In 2018, postcards were sent to potential transit users which likely targeted a greater number of occasional or low-occasional transit riders.

<rpc-chart url="able-to-travel.csv"
    chart-title="Are you able to travel everywhere you would like to within the community?"
    type="pie"
    stacked="true"
    aspect-ratio="4"></rpc-chart>

Listening to transit riders' overall satisfaction with the specialized transportation services system is paramount as it provides a direct and valuable feedback loop from the primary users of these specialized services. Rider satisfaction is a key indicator of the effectiveness and quality of the specialized transportation system. Understanding the factors that contribute to riders' contentment or dissatisfaction helps transportation authorities pinpoint areas that require improvement or further investment. A high level of rider satisfaction not only indicates a well-functioning specialized transportation system but also fosters a positive perception of these services, encouraging more people with disabilities and low incomes to utilize them as their preferred mode of transit.

<rpc-chart url="satisfaction-level.csv"
    chart-title="What is your overall satisfaction with the transportation services you utilize?"
    type="horizontalbar"
    stacked="true"
    aspect-ratio="4"></rpc-chart>

Older adults, people with disabilities, and people with low incomes often face common barriers to mobility that hinder their access to transportation and limit their ability to participate fully in society. One significant challenge is limited access to affordable and reliable transportation options. Many older adults and people with low incomes may not have access to private vehicles, and public transportation services may not adequately cover their specific needs or operate in their preferred areas. Additionally, people with disabilities may encounter physical barriers, such as lack of wheelchair accessibility or inadequate accommodation on public transportation vehicles, which restrict their mobility. Financial constraints further exacerbate the issue, as the cost of transportation fares and specialized services can be prohibitive for those on limited incomes. These combined barriers create a cycle of limited mobility, isolating these vulnerable groups and restricting their ability to engage in essential activities like accessing healthcare, education, employment opportunities, and participating in social interactions. Addressing these challenges requires comprehensive and inclusive transportation policies that prioritize affordability, accessibility, and responsiveness to the diverse needs of older adults, people with disabilities, and people with low incomes.

In 2018, when only four answer choices were given for this question, close to 90 percent of survey respondents were either very satisfied or satisfied with their transit service. In 2023, a fifth answer choice was inserted and there was a greater proportion of respondents who selected '2' as their satisfaction level. This could indicate that satisfaction has decreased, or that survey respondents would've chosen a more moderate answer choice in 2018 if it had been made available.

<rpc-chart url="barriers.csv"
    chart-title="What do you see as the greatest barrier to mobility in Champaign-Urbana?"
    type="pie"
    stacked="true"
    aspect-ratio="4"></rpc-chart>

Age and transit are closely related as peoples' transportation needs and preferences often change with age. As people grow older, their mobility requirements may shift due to factors such as physical limitations, health conditions, and lifestyle changes. Younger people may rely heavily on public transportation or rideshare services for their daily commute or social activities. As they age, some may transition to using specialized transportation services or accessible options to accommodate mobility challenges. Additionally, older adults might prefer more flexible and convenient transit options that cater to their specific needs, such as door-to-door services or reduced walking distances to transit stops. Transportation policies and infrastructure that consider the evolving needs of different age groups are essential in creating a comprehensive and inclusive transit network that supports people at every stage of life.

In the 2018 iteration of this plan, large proprtions of respondents noted that a lack of information and limited hours of operation were the biggest barriers to mobility. In 2023, lack of information tied with prohibitive advance notice requirements as the biggest barriers to mobility. To reflect this survey finding, addressing service hours and advance notice requirements were prioritized in the 'Mobility Tomorrow' section of this plan.

<rpc-chart url="age.csv"
    chart-title="What is your age?"
    type="pie"
    stacked="true"
    aspect-ratio="4"></rpc-chart>

Disability and transit are intrinsically linked as the accessibility and inclusivity of public transportation directly impact the mobility and independence of people with disabilities. Accessible transportation is essential for people with disabilities to participate fully in society, access education, employment, healthcare, and engage in social activities. Specialized features like wheelchair ramps, low-floor buses, and audio announcements are critical in ensuring that people with mobility, visual, or hearing impairments can navigate public transit with ease. Moreover, transportation policies that consider the diverse needs of people with disabilities play a pivotal role in promoting equal opportunities and breaking down barriers to community engagement. 

The survey respondent age breakdowns are similar in 2023 and 2018.

<rpc-chart url="disability.csv"
    chart-title="Do you have a physical disability?"
    type="pie"
    stacked="true"
    aspect-ratio="4"></rpc-chart>

Being aware of where a lack of transportation has hindered someone's livelihood is crucial because it sheds light on the far-reaching impact of transportation disparities on peoples' lives. Access to reliable transportation is a fundamental determinant of economic opportunity and social well-being. When people face challenges in commuting to work, accessing education, or reaching essential services, their livelihoods are significantly affected. Limited transportation options can lead to missed job opportunities, reduced income potential, and increased isolation, particularly for those living in rural and underserved areas. Understanding these instances helps inform policymakers, transportation authorities, and community organizations about the urgent need to address transportation inequalities. By identifying specific areas where peoples' livelihoods are compromised, targeted interventions can be developed, such as expanding public transportation routes, implementing specialized services, or providing financial assistance to alleviate the burden and foster more equitable access to mobility. Ultimately, being aware of the impact of transportation limitations empowers efforts to create a more inclusive and supportive transportation network that uplifts people and strengthens communities.

The survey respondent disability status breakdowns are similar in 2023 and 2018.

<rpc-chart url="lack.csv"
    chart-title="Has a lack of transportation kept you from medical appointments, meetings, work, or from something else you needed for daily living?"
    type="pie"
    stacked="true"
    aspect-ratio="4"></rpc-chart>

Community centers, senior centers, and organizations that cater to people with disabilities often serve as reliable hubs for disseminating information on accessible and specialized transportation options. Many older adults and people with low incomes rely on word-of-mouth recommendations from friends, family members, or neighbors who have experienced and utilized transportation services firsthand. Additionally, transit agencies and transportation providers often distribute informational materials at places frequented by these groups, such as healthcare facilities or social service centers. In the digital age, online platforms, websites, and social media groups also play a significant role in connecting people with relevant transportation information.

This survey question wasn't asked in 2018, but it is notable that over a quarter of respondents noted that a lack of transportation has kept them from necessities for daily living.

<rpc-chart url="find-info.csv"
    chart-title="Where do you find information about transportation services in the community?"
    type="pie"
    stacked="true"
    aspect-ratio="4"></rpc-chart>

## Appendix B: List of acronyms
**ACS:** American Community Survey<br>
**ADA:** Americans with Disabilities Act of 1990<br>
**AOA:** Administration on Aging<br>
**CCAM:** Coordinating Council on Access and Mobility<br>
**CCRPC:** Champaign County Regional Planning Commission<br>
**CIL:** Center for Independent Living<br>
**CUMTD:** Champaign-Urbana Mass Transit District<br>
**CUUATS:** Champaign-Urbana Urban Area Transportation <br>
**CVP:** Consolidated Vehicle Procurement<br>
**DOAP:** Downstate Operating Assistance Program<br>
**DOT:** Department of Transportation<br>
**ECIAAA:** East Central Illinois Area Agency on Aging<br>
**FAST Act:** Fixing America's Surface Transportation Act<br>
**FTA:** Federal Transit Adminstration<br>
**GATA:** Grant Accountability and Transparency Act<br>
**HSTP:** Human Services Transportation Planning<br>
**IDHFS:** Illinois Department of Healthcare and Family Services<br>
**IDOT:** Illinois Department of Transportation<br>
**MAP-21:** Moving Ahead for Progress in the 21st Century<br>
**MPO:** Metropolitan Planning Organization<br>
**NAICS:** North American Industry Classification System<br>
**NEMT:** Non-Emergency Medical Transportation<br>
**NETSPAP:** Non-Emergency Transportation Services Prior Authorization Program<br>
**OAA:** Older Americans Act of 1965<br>
**OIPI:** Office of Intermodal Project Implementation (IDOT)<br>
**OMA:** Open Meetings Act<br>
**SAFETEA-LU:** Safe, Accountable, Flexible, Efficient Transportation Equity Act: A Legacy for Users<br>
**Section 5307:** Urban Area Formula Grants<br>
**Section 5310:** Enhanced Mobility of Seniors and Individuals with Disabilities<br>
**Section 5311:** Formula Grants for Rural Areas<br>
**SILC:** Statewide Independent Living Council<br>
**TIP:** Transportation Improvement Plan<br>
**UIUC:** University of Illinois Urbana-Champaign<br>
**VMT:** Vehicle Miles Traveled<br>

## Appendix C: Transportation consumer survey

<iframe src="2023 UA-HSTP Consumer Survey.pdf" width="100%" height="600px"></iframe>

## Appendix D: Transportation provider survey

<iframe src="2023 UA-HSTP Provider Survey.pdf" width="100%" height="600px"></iframe>