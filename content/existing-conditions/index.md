---
title: Existing Conditions
draft: false
menu: main
weight: 20
summary: "Accounting for demographic information when creating a plan for human service transportation is vital to ensure inclusivity and effectiveness. Demographic factors such as age, disability, income level, and race significantly influence transportation needs and preferences. This section will summarize the most up-to-date demographic data for the Champaign-Urbana Urban Area."
bannerHeading: Existing Conditions
bannerText: This page displays a summary of up-to-date demographic data for the Champaign-Urbana Urban Area.
bannerUrl: banner.png
---
Accounting for demographic information when creating a plan for human service transportation is vital to ensure inclusivity and effectiveness. Demographic factors such as age, disability, income level, and race significantly influence transportation needs and preferences. This section will summarize the most up-to-date demographic data for the Champaign-Urbana Urban Area. 

## Study Area Demographics 
The study area for this plan is the Champaign-Urbana Urban Area, which includes the Cities of Champaign and Urbana, and the Villages of Savoy and Bondville in Champaign County, Illinois. The Champaign-Urbana Urban Area has an estimated population of 136,998 and covers nearly 40 square miles, according to 2023 ACS data, which was released on June 15th, 2023. 

Careful analysis of the mobility needs by each population and potential ridership of transit services, is key in developing and evaluating transit plans. As part of the plan development process, census data was collected to determine where in the Urban Area there are large numbers of certain target groups, including: 
- Older adults (60+) 
- People with disabilities 
- People and families with low-income 
- Zero-vehicle households 
- Black and African-American populations
- Hispanic and Latino/a populations 

## Transit-Dependent Populations 

**Youth:** People under the age of 16 do not have driver's licenses to transport themselves. Many young people also do not have the funds for their own transportation because they attend school full-time, limiting their potential work hours. Parents do not always have time to transport a young person when they need to go somewhere because the parents work. 

**Older adults (60+):** Older adults often have difficulties driving themselves due to limited physical capabilities, such as troubles seeing and hearing, stiff joints and muscles, slower reaction times, and dementia, among other challenges. Elderly individuals also face poverty at higher levels. According to a 2016 National Council on Aging study, over 25 million Americans aged 60 years or more are economically insecure, meaning they live at or below 250% of the federal poverty level (250% of the poverty level is equal to $34,450 per year for a single person). In addition, 23% of married Social Security recipients and 46% of single recipients aged 65 or over depend on Social Security for 90% or more of their income. This leaves older adults with almost zero disposable income for transportation. 

**Population below poverty:** This data is based on the federal poverty threshold per family unit size. The Office of the Federal Register set the 2018 HHS Poverty Guideline at an annual income of $14,580 for one person under the age of 65. A low-income individual is defined as an individual whose family income is at or below 150 percent of the poverty line. People with low incomes struggle to provide basic needs for themselves and their families, which leaves little to no funds for personal transportation. Public transportation is vital for people with low incomes because it is substantially cheaper than car ownership and is often safer, more convenient, and more comfortable than walking or biking.

**People with one or more disabilities:** Many community members have some form of disability, or may have different needs as they get older. Whether that poses challenges with physical mobility, sight or vision, or cognitive processing, there can be accessibility implications. Oftentimes, those needs can be made with reasonable accommodations on public transit; other times, passengers may need the specialized skills or equipment of a wheelchair accessible vehicle.
Zero- or one-vehicle households: Households with zero or one vehicle often depend on public transit to meet their mobility needs. People who fall under any of the transit-dependent target populations rely on public transportation to continue an independent lifestyle. Additionally, residents may represent more than one of these categories, solidifying their need for public transit. The City of Champaign and City of Urbana in particular face higher rates of poverty and zero-vehicle households due to the University of Illinois Urbana-Champaign student population. The most common reasons that people either don’t have a vehicle or only have one vehicle include physical or cognitive disabilities, the financial burdens and liabilities of car ownership, lack of a driver’s license, or a personal choice to remain car-free. 

The most recent demographic data comes from the 2023 American Community Survey administered by the United States Census Bureau and is available at the block group level for most demographics. This data is discussed in detail below with a focus on the general distribution of transit-dependent populations including youth, seniors, people with mobility limitations, people living below the poverty line, and households with zero vehicles available. 

## Age 
The total 2023 population of youth aged 0-17 years in the City of Champaign was 15,665, representing 17.7% of the city’s total population. The City of Urbana reported 4,531 youth (11.6%). The Villages of Savoy and Bondville reported 21.4% and 13.9% youth, respectively.

Older adults (people aged 60 or over) represent 14.7% (12,978) of the total population in the City of Champaign. The City of Urbana also reported 15.0% (5,872) of its population is aged 60 and over. The Villages of Savoy and Bondville reported older adult populations of 23.7% and 24.1%, respectively. The map below illustrates the distribution of older adults by block group across all the Champaign-Urbana Urban Area municipalities. Block groups with the highest percent of seniors correspond to the areas around the Urbana Country Club, Lincolnshire Fields Country Club, southeast Urbana, and north Savoy. 

<iframe src=" https://maps.ccrpc.org/distribution-of-older-adults-in-the-urban-area/" width="100%" height="600" allowfullscreen="true"></iframe>

## Population with Disabilities
Over eight percent of the Urban Area residents have at least one disability. ACS data for individuals with disabilities is not available at the block group level for mapping purposes. The map below shows the spatial distribution of households with at least one person with a disability. 

The Douglass Park neighborhood in Champaign has one of the blocks with the highest percentage of households with an occupant with a disability. The area in Champaign near University and Mattis is the other neighborhood with one block that has a very high percentage of households with an occupant with a disability. Other zones with similar disability rates include the Garden Hills/Dobbins Downs neighborhood and Southeast Urbana.

The Urban Area has 11,516 community members with disabilities, or 8.5% of the total population. Among older adults alone (those aged 65 or older), 4,005 have at least one disability., The Urban Area  has 14,925 adults aged 65 or over, 26.8% of whom have at least one disability. 

<iframe src=" https://maps.ccrpc.org/disabilitymap2023/" width="100%" height="600" allowfullscreen="true"></iframe>

## Population with Low Income
People with low-income tend to depend on transit to a greater extent than people with higher incomes. According to the American Community Survey, the average poverty threshold for a one-person household under the age of 65 was $12,880, and the average poverty threshold for a family of four people was $26,500 in 2021. The map below shows all households with income below the poverty threshold in the Urban Area, based on family size. 

The areas with the highest number of households living below the poverty level are North Lincoln Avenue near I-74, the University of Illinois campus near West Urbana and Midtown Champaign, and Northeast Urbana by AMBUCS Park, Carriage Estates Mobile Homes, and Chief Illini Village. The university student population accounts for much of the low-income demographic, and typically occupies regions such as Campustown, Midtown, part of West of Urbana, and North Lincoln Avenue. Non-student households that live below the poverty level tend to reside in Northeast Urbana between Cunningham Avenue and US 150. 

Information for individuals with low-income is not available at the block group level for mapping purposes. The map below instead shows the spatial distribution of households with low-income. 

The 2023 per capita income for the Urban Area was $31,060, according to the Census. This figure is lower than the per capita income for Champaign County ($32,971), and is much lower than the state and national averages, which are $39,571 and $37,638 respectively. 23.6% of the population in the Champaign-Urbana Urban Area lives below the poverty line. This contrasts with 15.0% for Champaign County, 12.1% for Illinois, and 11.6% for the United States as a whole. The City of Champaign and City of Urbana have higher rates of poverty, partly due to the University of Illinois at Urbana-Champaign student population. The map below shows the disaggregated the low-income population by geographical area that 27.1% of Urbana’s population is low-income, followed by 23.5% in Champaign, 5.3% in Bondville, and 11.5% in Savoy. 

<iframe src=" https://maps.ccrpc.org/distribution-of-low-income-households-in-the-urban-area/" width="100%" height="600" allowfullscreen="true"></iframe>

## Zero-Vehicle Households
The map below shows the distribution of zero-vehicle households. The 2021 ACS data indicates that 5,176 out of 35,206 households in the City of Champaign did not have a vehicle in 2021, representing about 14.7%. Approximately 19.7% (3,164) of Urbana’s 16,022 households were without vehicles. Of Savoy’s 3,461 households, 6.1% (212) did not have a vehicle. Bondville had a similarly low rate of zero-vehicle households; zero of Bondville’s 196 homes (0.0%) did not have a vehicle. 

The distribution of zero-vehicle households follows some trends similar to that of low-income households. The University of Illinois campus and Midtown Champaign have some of the highest percentages of households without a vehicle. The northeast Urbana area includes a high population of households living below the poverty level and a high population of zero-vehicle households. 

<iframe src=" https://maps.ccrpc.org/distribution-of-zero-vehicle-households-in-the-urban-area/" width="100%" height="600" allowfullscreen="true"></iframe>

## Minority Communities
### Hispanic or Latino/a Population
Hispanic/Latino/a residents make up 6.6% (5,837) of the City of Champaign’s population, according to the 2023 American Community Survey. The City of Urbana had a Hispanic population of 3,219 (8.2%). The Villages of Savoy and Bondville are 4.6%, 0.4% and 0.0% Hispanic, respectively. The map below illustrates the distribution of Hispanics by block group across all municipalities. 

Areas with a considerable Hispanic or Latino/a population include the two block groups: in Champaign, the Shadowwood Mobile Home neighborhood, and in Urbana, the Ivanhoe Estates Mobile Home Park and Willow Springs neighborhood, bound by I-74, Cunningham Avenue, and East Perkins Road. These block groups have approximately sixty percent of their population from Hispanic or Latino/a origin. The area along Cunningham Avenue between I-74 and Airport Road (which includes Northwood Mobile Home Park) has the next highest percentage at nearly sixteen percent Hispanic or Latino/a population. 

<iframe src=" https://maps.ccrpc.org/distribution-of-hispanic-or-latino-households-in-the-urban-area/" width="100%" height="600" allowfullscreen="true"></iframe>

### Black or African American Population
The City of Champaign has an African American population of 15,741 (17.8%). The City of Urbana’s African American population of 6,635, represents 16.9% of its total population. The Villages of Savoy and Bondville reported 5.1% and 7.6%, respectively. 

The following areas have large populations (>80% Black or African-American) of Black or African American residents: Garden Hills in Champaign; and the Lierman Neighborhood, parts of Country Squire, and parts of Lincolnwood in Urbana. 

The following areas have moderate populations (40-60% Black or African-American) of Black or African American residents: Bristol Park, Garden Park, Garwood, Holiday Park subdivision near Kenwood School, and the neighborhoods around Kaufman and Spalding Parks in Champaign; and the neighborhoods around Crystal Lake Park and King Park, the mobile home neighborhoods between I-74 and US 150, and Scottswood in Urbana. 

<iframe src=" https://maps.ccrpc.org/distribution-of-black-or-african-american-households-in-the-urban-area/" width="100%" height="600" allowfullscreen="true"></iframe>

## Aggregated Demographics
To locate the areas within the Urban Area that have the greatest transit-dependency, z-scores were used to standardize the data from each of the target populations. Z-scores enable the comparison between datasets from varied scales. It is a dimensionless number representing the distance a value is away from the mean, or average, following the formula below:

 z = (X - μ) / σ 

Where:
z = z-score 
X = original sample value in the dataset 
μ = the average 
σ = the standard deviation of the dataset 

Z-scores are closely tied to standard deviations: a z-score of one means the original value is a distance of one standard deviation positive or negative away from the average. A range of z-scores from negative two to two is associated with ninety-five percent of the data, or two standard deviations from the average. Similarly, positive z-scores above a value of two represent the top 2.5 percent of the data. The map below visualizes the summation of above average z-scores for all the Urban Area target populations. 

Within the Champaign-Urbana Urban Area, locations with the greatest transit-dependency include Garden Hills, Midtown Champaign, the North Lincoln Avenue, Southeast Urbana, and the east central Savoy neighborhoods of Winfield Village, The Place at 117 apartments, and Prairie Fields subdivision.

<iframe src=" https://maps.ccrpc.org/z-score-map/" width="100%" height="600" allowfullscreen="true"></iframe>
