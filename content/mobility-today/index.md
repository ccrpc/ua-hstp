---
title: Mobility Today
draft: false
menu: main
weight: 30
summary: "This section provides an overview of the human services transportation options available in our community, including paratransit, non-emergency medical transportation, and other specialized transportation services."
bannerHeading: Mobility Today
bannerText: This section outlines the various human services transportation options in the Champaign-Urbana Urban Area.
bannerUrl: banner.jpg
---
For many individuals with physical or cognitive disabilities, older adults, and those with serious health conditions, transportation can be a significant challenge. Accessing medical appointments, community resources, social events, and other appointments for daily life may be difficult or even impossible without specialized transportation services. In the Champaign-Urbana urbanized area, there is a range of human services transportation options that cater to the unique needs of these populations. This section will provide an overview of the general and human services transportation options available in our community, including paratransit, non-emergency medical transportation, and other specialized transportation services. By understanding these transportation options, residents of our community can better connect with resources and services necessary for living a fulfilling and independent life.

#### Resources mentioned in this section:
- [Champaign-Urbana Mass Transit District (MTD)](https://mtd.org/)
- [MTD ADA Paratransit FAQ](https://mtd.org/riding/mtd-services/can-you-tell-me-about-your-ada-services/)
- [Champaign County Area Rural Transit System (C-CARTS)](https://c-carts.com/)
- [Champaign County Rural Transit Advisory Group (RTAG)](https://www.co.champaign.il.us/countyboard/Meetings_RTAG.php)
- [HSTP Transportation Directory](https://ccrpc.org/programs/hstp/human-service-transportation-plan-region-8-directory/)
- [2023 Trends Report](https://issuu.com/ccountychamber/docs/trends_report_2023)
- C-CARTS phone: (217) 344-4287
- MTD ADA Paratransit phone: (217) 384-8188

## Existing transportation services
This section will outline the various transportation options available in the Champaign-Urbana urbanized area, including public transit for the general public, specialized services for specific communities, and private transportation companies. 

### Public transportation
The Champaign-Urbana urbanized area is served by two public transportation providers: the [Champaign-Urbana Mass Transit District (MTD)](https://mtd.org/) and the [Champaign County Area Rural Transit System (C-CARTS)](https://c-carts.com/). 

Regular transit service within the urbanized area is covered by MTD’s fixed-route service. MTD operates 20 unique routes throughout the cities of Champaign and Urbana and the Village of Savoy, charging $1 per one-way trip. Of those routes, five offer late night service. Each MTD bus is equipped with an ADA lift and on-board wheelchair securements. MTD’s service is overseen by the MTD Board.

In addition to its fixed-route service, MTD operates a complementary ADA Paratransit service for people with disabilities who are unable to use the fixed-route service. The service operates within 3/4 miles of each existing fixed route. To request a ride through [MTD’s ADA Paratransit service](https://mtd.org/riding/mtd-services/can-you-tell-me-about-your-ada-services/), users must make a reservation at least one day prior to the requested service. In addition, all eligible riders must apply and be registered prior to the initial ride. Prospective riders can learn more about MTD’s ADA Paratransit service by calling (217) 384-8188.

The Champaign County Area Rural Transit System (C-CARTS) provides fixed-route and demand-response public rural transportation throughout Champaign County, Monday through Friday from 6:00 a.m. to 6:00 p.m. C-CARTS can transport riders from inside the urbanized area to a location outside the urbanized area or vice versa, but, due to state regulations, it cannot make a trip where both the origin and destination are inside the urbanized area. Although C-CARTS is categorized as a rural service, many people use the service to travel from outlying rural parts of Champaign County to the urbanized area for medical appointments, work, school, shopping, recreation, and social services. Additionally, many riders use the service to travel from Champaign-Urbana and parts of Rantoul to the factory campus on the western edge of Rantoul. 

C-CARTS passengers who qualify for ADA Paratransit service may be accompanied by a Personal Care Attendant (PCA), who rides at no cost. The Champaign County Rural Transit Advisory Group (RTAG) oversees C-CARTS affairs, and MTD operates the system. To reserve a ride through C-CARTS, call (217) 344-4287.

### Rantoul service contract for Eagle Express and Rantoul Connector
In September 2016, Champaign County and the Champaign-Urbana Mass Transit District expanded C-CARTS service into Rantoul, Illinois with the creation of two fixed routes: The Eagle Express, which operates within Rantoul, and the Rantoul Connector, which runs between Champaign-Urbana and Rantoul. Both routes are active between the hours of 6:00 a.m. to 6:00 p.m. on Monday through Friday. Each vehicle complies with ADA standards with a lift and ramp for those who need it. Find route maps and additional riding information on the C-CARTS homepage or by calling (217) 344-4287.

### Private transportation providers
Private taxis, ambulances, and rideshare companies like Uber and Lyft also offer specialized transportation services within and around the urbanized area. Intercity buses and commuter rail, such as Peoria Charter Coach and Amtrak, respectively, connect the urbanized area to other cities and destinations. For a complete list of all transportation options in and around Champaign-Urbana, visit the [HSTP Transportation Directory](https://ccrpc.org/programs/hstp/human-service-transportation-plan-region-8-directory/).

### Other transportation providers
Some human service agencies, most commonly adult day care facilities and senior living communities, provide exclusive transportation services catered to the needs of their clients. These services fill in for unmet needs that aforementioned public and private transportation services may not be able to accommodate. Human service agencies are those which support people with cognitive disabilities, people with physical disabilities, and older adults; they are not intended for use by the general public.

Other transportation providers include K-12 student transportation (yellow buses) and shuttle services provided by certain apartment complexes and churches. Since these services aren’t open to the general public, they are not included in the HSTP Transportation Directory.

## Major trip generators
When designing and updating a transportation service model, providers pay close attention to which destinations are most popular among riders. This creates a more efficient flow of operations for the provider, and it also ensures that riders have easy access to our community’s biggest employment, healthcare, and educational centers. 

Data in the following table is based on survey responses to the Champaign County Regional Planning Commission’s Long Range Transportation Plan outreach campaign, carried out between April and September of 2023.

# Major Trip Generators in the Champaign-Urbana Urban Area
1. University of Illinois Urbana-Champaign (educational institution)
2. Meijer (grocery store)
3. Carle Foundation Hospital (health services)
4. Champaign-Urbana Mass Transit District (transportation services)
5. Parkland College (educational institution)

## Top employers
Employment in the Champaign-Urbana urbanized area is dominated by the higher education and healthcare sectors. Most of Champaign County’s top 25 employers are located primarily in the urbanized area, though some have satellite branches in non-urbanized Champaign County. These 21 organizations  employ more than 32,000 people. The University of Illinois is the largest employer (14,817 employees), followed by Carle Foundation Hospital (6,438 employees). Other top employers include Champaign Unit 4 School District (2,088 employees), Champaign County (1,173 employees), and Urbana School District #116 (1,044 employees).

Data in the following table is based on the [2023 Trends Report](https://issuu.com/ccountychamber/docs/trends_report_2023) published by the Champaign County Chamber of Commerce.

<rpc-table url="top-employers-table.csv"
  table-title="Top Employers in the Champaign-Urbana Urban Area"
  text-alignment="l,r"></rpc-table>