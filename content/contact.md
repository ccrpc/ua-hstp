---
title: Contact
draft: false
menu: eyebrow
weight: 10
---
### Human Service Transportation Planning (HSTP) Coordinator
If you have questions or comments on this plan, contact Mimi Hutchinson (ehutchinson@ccrpc.org).

### Other Useful Contacts

#### HSTP Coordinator/PCOM
Emma Woods<br>
*HSTP Coordinator/PCOM*
ewoods@ccrpc.org<br>
(217) 531-8285<br>

#### Champaign County Area Rural Transit System (C-CARTS)
Klentoria Lee-Clements<br>
*Special Services Manager*<br>
klclements@mtd.org<br>
(217) 344-4287<br>

#### MTD ADA Paratransit
Klentoria Lee-Clements<br>
*Special Services Manager*
klclements@mtd.org<br>
(217) 384-8188<br>

#### University of Illinois Disability Resources and Education Services 
Mylinda Netherton<br>
*Accessibility and Transportation Coordinator*<br>
mylindag@illinois.edu<br>
(217) 244-4104<br>

#### Champaign-Urbana Special Recreation (CUSR)
Rachel Voss<br>
*Marketing Manager*<br>
rachel.voss@champaignparks.org<br>
(217) 819-3932<br>