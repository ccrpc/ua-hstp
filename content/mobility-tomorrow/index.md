---
title: Mobility Tomorrow
draft: false
menu: main
weight: 60
summary: "The goals in this section provide a clear direction for policymakers, planners, and service providers, ensuring that the transportation needs of individuals with disabilities, older adults, and people with low incomes are met effectively."
bannerHeading: Mobility Tomorrow
bannerText: This page displays goals and recommendations for human service transportation providers in the Champaign-Urbana Urban Area.
bannerUrl: banner.jpg
---

Setting goals and recommendations for human services transportation providers is part of establishing accessible, equitable, and efficient transportation systems for vulnerable and marginalized populations. These goals provide a clear direction for policymakers, planners, and service providers, ensuring that the transportation needs of individuals with disabilities, older adults, and people with low incomes are met effectively. By establishing well-defined objectives, such as increasing service coverage, reducing travel barriers, and enhancing service quality, human services transportation can become more accessible and convenient.

#### Resources mentioned in this section:
- ["GIS-Based Approach to Dynamic Accessibility: Incorporating a User Perspective to Recognize Social Equity Implications," 2022](https://journals.sagepub.com/doi/10.1177/03611981221150917)
- [Faith in Action](https://www.osfhealthcare.org/heart-of-mary/amenities/social-services/)

## Goal 1: Increase access to service by supporting same-day reservations
Same-day scheduling for on-demand transit holds exceptional significance, particularly within the context of older adults and people with disabilities. This approach offers a level of flexibility and autonomy that is essential for individuals facing mobility challenges. Many older adults and people with disabilities often encounter unexpected medical appointments, essential errands, and social engagements that require timely transportation solutions. Same-day scheduling empowers them to maintain an active and independent lifestyle, as it accommodates their spontaneous needs without the constraints of rigid pre-planned schedules. This mode of transportation enhances their quality of life by reducing isolation, ensuring timely access to medical care, and fostering social participation. By affording these individuals the ability to arrange transportation at their convenience, same-day scheduling within on-demand transit demonstrates a profound commitment to inclusivity and responsiveness in transportation services.

The Transportation Consumer survey results shed light on the preferences of human services transportation riders within our Urban Area, underscoring a strong inclination towards same-day reservations. The survey highlighted that a significant portion of respondents, including a large proportion of older adults and people with disabilities, expressed a preference for the flexibility and convenience that same-day scheduling offers. Participants cited the unpredictable nature of their daily needs, such as medical appointments and essential errands, as a key factor in favoring this approach. The data reinforces the notion that implementing same-day reservations would be a valuable step toward aligning transportation services more closely with the diverse and evolving needs of these populations, enhancing their overall transportation experience and promoting their well-being.

### Strategy
Develop an inventory of the economic and administrative obstacles hindering agencies' ability to allow same-day reservations. Share this inventory with IDOT.

### Performance measure(s)
Number of Urban Area HSTP providers offering same-day reservations.

### Needed resources
Operations costs, transit vehicles, staff time, resource reallocation.

## Goal 2: Increase service availability for late-shift workers and residents of the outlying community
Catering to late-shift workers is essential for fostering inclusivity and supporting our local economy. Late-shift workers, which in our community include healthcare professionals, hospitality staff, security personnel, and factory employees, play a vital role in maintaining our Urban Area's operations during all hours of the day. By offering reliable and accessible transit service during these times, cities demonstrate their committment to supporting the workforce's diverse needs. This can help alleviate traffic congestion during peak hours by distributing commute patterns more evenly. According to an article by UIUC researchers, "increasing accessibility has beneits both or individuals and for broader economic productivity" (["GIS-Based Approach to Dynamic Accessibility: Incorporating a User Perspective to Recognize Social Equity Implications," 2022](https://journals.sagepub.com/doi/10.1177/03611981221150917)).

### Strategy
Develop an inventory of the economic and administrative obstacles hindering agencies' ability to expand service hours. Share this inventory with IDOT.

### Performance measure(s)
Number of Urban Area HSTP providers offering service outside of traditional working hours.

### Needed resources
Operations dollars, transit vehicles, staff time, resource reallocation.

## Goal 3: Revive volunteer-based ridesharing services
In the aftermath of the COVID-19 pandemic, local volunteer-run ride-sharing services, namely [Faith in Action](https://www.osfhealthcare.org/heart-of-mary/amenities/social-services/), were forced to cease the provision of transportation to their clients. Faith in Action helped people age 55 and over maintain their mobility by providing rides within the community. While Champaign-Urbana's Faith in Action no longer provides transportation, they still provide help with shopping and errands, health education and exercise classes, and community events. Faith in Action also provides practical, emotional and spiritual assistance for older adults to help them live safe and independent lives.

Increasing participation in volunteer-run services in the Covid-19 era requires a multi-pronged strategy. Tailored communication campaigns should spotlight the positive community impact and personal fulfillment they can derive from volunteering. Secondly, addressing technological hesitations by offering user-friendly training sessions tailored to their comfort levels with smartphones and apps can help bridge the digital divide. Flexibility in scheduling and shorter time commitments can make volunteering more feasible for those with multiple responsibilities. Recognizing volunteers' contributions through awards and appreciation events not only boosts their morale but also encourages sustained engagement, aiding the recovery of these vital services. Lastly, ensuring safe and healthy environments for volunteers could help them feel more comfortable in the shadow of the pandemic.

### Strategy
Collaborate with Faith in Action leader(s) to re-establish transportation service. Encourage other local volunteer-led groups to establish transportation service. Aid in the creation and distribution of materials advertising these services.

### Performance measure(s)
Resumption of Faith in Action transportation service. Establishment of other volunteer-based transportation services in the Urban Area.

### Needed resources
Volunteer time, personal protective equipment (PPE), vehicles, volunteer coordination dollars.

## Goal 4: Re-institute trainings
In the realm of human service transportation, comprehensive training programs hold immense significance for both drivers and service providers. Hosting trainings was a goal listed in the 2018 update of this plan, but because of the pandemic, training opportunities were limited between 2018 and 2023. In the past, HSTP trainings have been held at the Champaign County Highway building, but no trainings have occurred since the Covid-19 pandemic. Potential themes for future trainings include travel training, training in the provision of transportation for people with disabilities, grant application how-tos, and vehicle inspection training.

Travel training equips drivers with the skills to navigate various routes efficiently, ensuring timely and safe journeys for passengers. Maintenance training empowers drivers to conduct routine checks, thereby enhancing the longevity and reliability of vehicles, which is crucial for maintaining consistent service. Moreover, specialized training in the transportation of individuals with disabilities fosters an inclusive environment, allowing drivers to cater to unique needs and ensure a dignified travel experience. Grant application training provides service providers with the tools to secure essential funding, enabling the sustainability and expansion of transportation services. Lastly, vehicle inspection training promotes safety by training drivers to identify and address potential issues proactively, reducing the risk of breakdowns and accidents. Collectively, these training components not only elevate the overall quality of human service transportation but also contribute to passenger satisfaction, operational efficiency, and the overarching goal of fostering accessible and reliable mobility for all.

### Strategy
Collaborate with staff at Illinois Terminal and/or the Champaign County Highway building to reserve time to host trainings. Reference training materials from pre-Covid. Pubilcize trainings among relevant groups. Host trainings.

### Performance measure(s)
Host at least 3 trainings in CY24.

### Needed resources
Staff time, meeting space, materials for print advertisements, spare transit vehicle for maintenance training.

## Goal 5: Leverage technology and local media to increase awareness of transit options
Harnessing technology and local media platforms is an important part of bolstering awareness of human service transportation options. The most recent survey data revealed that about a third of current human service transportation riders who responded to the most recent survey said there needs to be more information about human service transportation options in Champaign-Urbana. At the same time, close to 40% of survey respondents reported learning about Champaign-Urbana's transportation services via word of mouth and close to 25% said they learn about our community's transportation options online.

In an increasingly interconnected world, technology provides a dynamic avenue to disseminate information to a wider audience. Mobile apps, social media platforms, and online portals can be leveraged to effectively communicate available services, schedules, and booking procedures. This not only empowers potential users to access information at their fingertips but also enables real-time updates for changes in routes or timings. Simultaneously, local media, including local newspapers, radio stations, and bulletin boards, are integral to reaching segments of the population that might not be as digitally engaged. Remaining active in these avenues allows service providers to tailor their messages to the specific needs and preferences of their communities, fostering trust and encouraging uptake of these essential transportation options. 

### Strategy
Leverage connections with local human service and transportation providers to ensure that up-to-date advertisements are available across town. Develop an inventory of most recent human service transportation information and where it has been distributed.

### Performance measure(s)
Develop an editible advertising inventory and share it with human service transportation partners in the Urban Area. Request their help in keeping the inventory up-to-date.

### Budget implications
Staff time, online inventory platform.