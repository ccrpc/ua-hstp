---
title: Outreach Efforts
draft: false
menu: main
weight: 40
summary: "Over the past two months, a comprehensive survey initiative was undertaken in the Champagin-Urbana area, targeting specialized transportation providers. The survey was designed with inclusivity in mind, ensuring accessibility for all respondents."
bannerHeading: Outreach Efforts
bannerText: This section summarizes survey distribution and data collection efforts.
bannerUrl: banner.png
---
During May, June, July and August of 2023, a comprehensive survey initiative was undertaken in the Champaign-Urbana area, targeting specialized transportation consumers and providers. The survey was designed with inclusivity in mind, ensuring accessibility for all respondents. Multiple formats were prepared, including paper copies and a digital version that adhered to the Web Content Accessibility Guidelines (WCAG) and was screen reader-compatible. Additionally, advertising materials were strategically disseminated to promote awareness of the survey among the community. The primary focus of this survey was to gain insights into the perspectives of riders who utilize specialized transportation services in the area, with a specific emphasis on individuals with disabilities, older adults, and those with limited financial resources.

# Survey distribution
The first phase of the survey deployment involved placing physical copies inside various buildings and specialized transit vehicles throughout the community. This approach aimed to reach a wide range of potential respondents, ensuring representation from diverse backgrounds and needs. By distributing surveys in easily accessible locations, the initiative encouraged maximum participation and feedback from the target audience.

In the digital realm, the WCAG-compliant and screen reader-compatible version of the survey was made available online. This effort sought to remove any barriers that might prevent individuals with disabilities from participating in the survey. By prioritizing accessibility, the survey organizers aimed to gather the most comprehensive and representative data possible, ensuring that the voices of all riders were heard and considered.

The second phase of the survey involved publicizing the initiative through advertising materials. Flyers, posters, and online announcements were strategically placed throughout the community to raise awareness about the survey's importance and encourage participation. Additionally, outreach efforts were made to relevant community organizations, disability support centers, and low-income advocacy groups to ensure that the survey reached its target audience effectively.

Through this survey initiative, invaluable insights were gathered about the experiences and perceptions of specialized transportation services within the Champaign-Urbana area. By focusing on the perspectives of individuals with disabilities and low-income individuals, the survey aimed to identify areas of improvement and potential challenges faced by these specific groups. The collected data would serve as a foundation for future discussions and collaborations among stakeholders to enhance specialized transportation services, making them more inclusive, efficient, and responsive to the needs of the community. Ultimately, the survey's efforts will contribute to the creation of a more accessible and equitable transportation system that caters to the unique requirements of all its users.

The 'Transportation Consumer Survey' was distributed to users of transportation services offered by the following agencies and organizations:
- OSF Healthcare
- MTD Paratransit
- Developmental Services Center
- Champaign-Urbana Special Recreation
- UIUC Disability Resources and Educational Services (DRES)
- Family Service of Champaign County

The following organizations were involved in the 2018 iteration of the HSTP Urban Area plan but were not included in this year's iteration:
- AMT Ambulance
- OSF PRO Ambulance (now defunct)
- Carle Foundation Hospital
- American Cancer Society

Of all the organizations that partnered with RPC to distribute surveys from transportation consumers, OSF Heart of Mary Medical Center collected 2 from patrons of its facility, MTD ADA Paratransit collected 37 responses from riders on-board their vehicles, UIUC's Disability Resources and Educational Services collected 3 responses, DSC collected 1, and RPC staff collected 4 responses at various outreach events from throughout the summer of 2023. 11 transportation consumer surveys were filled out online and 39 were completed on a paper copy.

<rpc-chart url="response-venue-breakdown.csv"
    chart-title="Transportation Consumer Survey Response Breakdown by Organization"
    type="pie"
    stacked="true"
    aspect-ratio="4"></rpc-chart>

# Urban Area HSTP Consumer Survey

<iframe src="2023 UA-HSTP Consumer Survey.pdf" width="100%" height="600px"></iframe>

# Urban Area HSTP Provider Survey

<iframe src="2023 UA-HSTP Provider Survey.pdf" width="100%" height="600px"></iframe>